package com.amazon.components;

import com.amazon.beans.ProductBean;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class CartProducts extends QAFWebComponent{

	public CartProducts(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}

	@FindBy(locator = "link.productname.shoppingcartpage")
	private QAFWebElement linkProductname;
	
	@FindBy(locator = "lbl.productprice.shoppingcartpage")
	private QAFWebElement lblProductprice;

	public QAFWebElement getLinkProductname() {
		return linkProductname;
	}

	public QAFWebElement getLblProductprice() {
		return lblProductprice;
	}
	
	public boolean equals(Object obj)
	{
		if(obj!=null && obj instanceof String)
			return linkProductname.getText().equalsIgnoreCase((String)obj);
		if(obj!=null && obj instanceof ProductBean)
		{
			ProductBean prod=(ProductBean)obj;
			return linkProductname.getText().equalsIgnoreCase(prod.getName()) && lblProductprice.getText().equalsIgnoreCase(prod.getPrice());
		}
		return super.equals(obj);
	}
	
	
	
}
