package com.amazon.components;

import com.qmetry.qaf.automation.core.QAFCommandBean;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ProductListItem extends QAFWebComponent {

	public ProductListItem(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}

	@FindBy(locator = "hdr.productname.productlistpage")
	private QAFWebElement hdrProductname;
	
	@FindBy(locator = "lbl.sellername.productlistpage")
	private QAFWebElement lblSellername;
	
	@FindBy(locator = "lbl.price.productlistpage")
	private QAFWebElement lblPrice;

	public QAFWebElement getHdrProductname() {
		return hdrProductname;
	}

	public QAFWebElement getLblSellername() {
		return lblSellername;
	}

	public QAFWebElement getLblPrice() {
		return lblPrice;
	}
	
	public boolean equals(Object obj)
	{
		if(obj!=null && obj instanceof String)
		{
			return hdrProductname.getText().equalsIgnoreCase((String)obj);
		}
		return super.equals(obj);
	}
}
