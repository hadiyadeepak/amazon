package com.amazon.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ProductdetailPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "lbl.productname.productdetailpage")
	private QAFWebElement lblProductnameProductdetailpage;
	
	@FindBy(locator = "lbl.productprice.productdetailpage")
	private QAFWebElement lblProductpriceProductdetailpage;
	
	@FindBy(locator = "lbl.procuctbrand.productdetailpage")
	private QAFWebElement lblProcuctbrandProductdetailpage;

	@FindBy(locator="btn.addtocart.productdetailpage")
	private QAFWebElement btnAddToCart;
	
	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}
	
	public QAFWebElement getBtnAddToCart()
	{
		return btnAddToCart;
	}
	
	public void clickOnAddToCartBtn()
	{
		btnAddToCart.click();
	}

	public QAFWebElement getLblProductnameProductdetailpage() {
		return lblProductnameProductdetailpage;
	}

	public QAFWebElement getLblProductpriceProductdetailpage() {
		return lblProductpriceProductdetailpage;
	}

	public QAFWebElement getLblProcuctbrandProductdetailpage() {
		return lblProcuctbrandProductdetailpage;
	}

}
