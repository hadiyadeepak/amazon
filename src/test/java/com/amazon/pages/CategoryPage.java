package com.amazon.pages;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class CategoryPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "link.subcategory.categorypage")
	private QAFWebElement linkSubcategoryCategorypage;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getLinkSubcategoryCategorypage() {
		return linkSubcategoryCategorypage;
	}
	

	public void openSubCategory(String categoryName)
	{
		QAFExtendedWebElement element=new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("link.subcategory.categorypage"), categoryName));
		element.click();
	}
}
