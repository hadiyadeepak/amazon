package com.amazon.pages;

import java.awt.TrayIcon.MessageType;
import java.util.List;


import com.amazon.beans.ProductBean;
import com.amazon.components.ProductListItem;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class ProductlistPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "list.product.productlistpage")
	private List<ProductListItem> listProduct;
	
	@FindBy(locator = "hdr.productname.productlistpage")
	private QAFWebElement hdrProductname;
	
	@FindBy(locator = "lbl.sellername.productlistpage")
	private QAFWebElement lblSellername;
	
	@FindBy(locator = "lbl.price.productlistpage")
	private QAFWebElement lblPrice;

	@FindBy(locator="sel.sortproduct.productlistpage")
	private QAFWebElement selSortProduct;
	
	
	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	
	}

	
	public List<ProductListItem> getListProductProductlist() {
		return listProduct;
	}


	public QAFWebElement getHdrProductnameProductlist() {
		return hdrProductname;
	}


	public QAFWebElement getLblSellernameProductlist() {
		return lblSellername;
	}


	public QAFWebElement getLblPriceProductlist() {
		return lblPrice;
	}

	public QAFWebElement getSelSortProduct()
	{
		return selSortProduct;
	}
	
	public void waitForPageToLoad()
	{
		selSortProduct.waitForPresent();
	}

	public ProductBean selectProductByIndex(int index)
	{
		ProductListItem productItem=getListProductProductlist().get(index);
		productItem.getHdrProductname().click();
		String name=productItem.getHdrProductname().getText();
		String price=productItem.getLblPrice().getText();
		String seller=productItem.getLblSellername().getText();
		ProductBean productToSelect=new ProductBean(name,price,seller);
		return productToSelect;
	}
	
	public ProductBean selectProductByName(String prodName)
	{
		for(ProductListItem item:getListProductProductlist())
		{
			if(item.equals(prodName))
			{
				String name=item.getHdrProductname().getText();
				String price=item.getLblPrice().getText();
				String seller=item.getLblSellername().getText();
				ProductBean productToSelect=new ProductBean(name,price,seller);
				return productToSelect;
			}
		}
		Reporter.log(prodName + " not found ", MessageTypes.Fail);
		return null;
	}
}
