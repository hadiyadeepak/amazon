package com.amazon.pages;

import java.util.List;

import org.testng.Reporter;

import com.amazon.beans.ProductBean;
import com.amazon.components.CartProducts;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ShoppingcartPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "link.productname.shoppingcartpage")
	private QAFWebElement linkProductname;
	
	@FindBy(locator = "lbl.productprice.shoppingcartpage")
	private QAFWebElement lblProductprice;

	@FindBy(locator="div.addedproduct.shoppingcartpage")
	private List<CartProducts> divAddedProduct;
	
	public List<CartProducts> getdivAddedProduct()
	{
		return divAddedProduct;
	}
	
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getLinkProductname() {
		return linkProductname;
	}

	public QAFWebElement getLblProductprice() {
		return lblProductprice;
	}
	
	public boolean isProductInCart(String product)
	{
		for(CartProducts actualProduct:divAddedProduct)
		{
			if(actualProduct.equals(product))
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean isProductInCart(ProductBean product)
	{
		Reporter.log("Expected product name = "+product.getName()+ " AND price = "+product.getPrice()	, true);
		for(CartProducts cartProduct:divAddedProduct)
		{
			Reporter.log("Actul product name = "+cartProduct.getLinkProductname().getText()+" AND price = "+cartProduct.getLblProductprice(),true);
			if(cartProduct.equals(product))
			{
				return true;
			}
		}
		return false;
	}

}
