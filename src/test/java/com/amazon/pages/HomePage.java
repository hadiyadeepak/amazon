package com.amazon.pages;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "link.shopbycategory.homepage")
	private QAFWebElement linkShopbycategory;
	
	@FindBy(locator = "link.category.homepage")
	private QAFWebElement linkCategory;
	
	@FindBy(locator = "link.cart.homepage")
	private QAFWebElement linkCart;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
		
		driver.get("/");
		driver.manage().window().maximize();
	}

	public QAFWebElement getLinkShopbycategoryHomepage() {
		return linkShopbycategory;
	}

	public QAFWebElement getLinkCategoryHomepage() {
		return linkCategory;
	}

	public QAFWebElement getLinkCartHomepage() {
		return linkCart;
	}
	
	public void clickOnShopByCategoryLink()
	{
		linkShopbycategory.click();
	}
	
	public void verifyHomePage(String expectedTitle)
	{
		Validator.verifyThat(driver.getTitle(), Matchers.hasToString(expectedTitle));
	}
	
	public void launhApp()
	{
		openPage(null);
	
	}
	
	public void selectCategory(String category)
	{
		new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("link.category.homepage"), "Televisions")).click();
	}

	public void openShoppingCart() {
		// TODO Auto-generated method stub
		linkCart.click();
	}
	

}
