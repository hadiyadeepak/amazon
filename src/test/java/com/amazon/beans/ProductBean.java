package com.amazon.beans;

public class ProductBean {
	
	private String name;
	private String price;
	private String seller;

	public ProductBean(String name,String price,String seller)
	{
		this.name=name;
		this.price=price;
		this.seller=seller;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getSeller() {
		return seller;
	}

	public void setSeller(String seller) {
		this.seller = seller;
	}
	
	
}
