package com.amazon.utilities;

import com.qmetry.qaf.automation.ui.WebDriverTestBase;

public class CommonUtils {

	public static void switchToWindow()
	{
		String parentWindow=new WebDriverTestBase().getDriver().getWindowHandle();
		for(String window:new WebDriverTestBase().getDriver().getWindowHandles())
		{
			if(!window.equals(parentWindow))
				new WebDriverTestBase().getDriver().switchTo().window(window);
		}
	}
	
}
