package com.amazon.tests;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import com.amazon.beans.ProductBean;
import com.amazon.pages.CategoryPage;
import com.amazon.pages.HomePage;
import com.amazon.pages.ProductdetailPage;
import com.amazon.pages.ProductlistPage;
import com.amazon.pages.ShoppingcartPage;
import com.amazon.utilities.CommonUtils;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Validator;

public class VerifyProductTest extends WebDriverTestCase{


	@Test
	public void verifyProduct()
	{
		HomePage hp=new HomePage();
		hp.launhApp();
		hp.verifyHomePage("Online Shopping site in India: Shop Online for Mobiles, Books, Watches, Shoes and More - Amazon.in");
		hp.clickOnShopByCategoryLink();
		hp.selectCategory("Televisions");
		CategoryPage cp=new CategoryPage();
		cp.openSubCategory("Sony TVs");
		ProductlistPage list=new ProductlistPage();
		list.waitForPageToLoad();
		ProductBean selectedProduct=list.selectProductByIndex(0);
		CommonUtils.switchToWindow();
		
		ProductdetailPage productDetail=new ProductdetailPage();
		productDetail.waitForPageToLoad();
		productDetail.getLblProductnameProductdetailpage().verifyText(selectedProduct.getName());
		productDetail.clickOnAddToCartBtn();
		
		hp.openShoppingCart();
		
		ShoppingcartPage cartPage=new ShoppingcartPage();
		cartPage.waitForPageToLoad();
		Validator.verifyThat(cartPage.isProductInCart(selectedProduct),Matchers.equalTo(true));
		
	}
}
